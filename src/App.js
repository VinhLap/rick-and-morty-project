import React from 'react';
import './App.css';
import Main from './components/Main/Main.js';
import Profile from './components/Profile/Profile.js'
import logo from '../src/components/Main/RMLogo.jpg.png'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

function App() {
  return (
    
    <Router>
      <img id='logo' src={logo} alt='logo' />
      <div className="App">
        <Switch>
            <Route path="/profile/:id" component={Profile} />
            <Route path="/:pageNr" component = {Main}/>
            <Route path="/" component = {Main}/>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
