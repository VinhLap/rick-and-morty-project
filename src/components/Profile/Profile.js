import React from 'react';
import { Link } from 'react-router-dom';
import loadingLogo from './head.jpg'
import styles from './Profile.css'
import logo from '../Main/RMLogo.jpg.png'

class Profile extends React.Component{
    constructor(props){
        super(props);
        this.state={
            id: this.props.match.params.id,
            card: null,
        }
        console.log("CONTSRTUCOR")
    }
    async componentDidMount(){
        try {
            const response = await (await fetch(`https://rickandmortyapi.com/api/character/${this.state.id}`)).json();
            this.setState({
              card: response});
            console.log("COMPMOUNT: " +this.state.card.name)
          } catch (e) {
            console.error(e);
          }
    }
    render(){
        console.log("RENDER" + this.props.match.params.id)
        const thisCard = this.state.card;
        if(thisCard == null){
            return(
                <img id='loader' src={loadingLogo} alt="loading" />
            )
        }else{
            return(
                <div>
                    <Link to={`/`}>
                        <button id='btnBack'>Back</button>
                    </Link>
                    <div id='cardBody'>
                        <div id="specCard"> 
                            <img id ='image' src={thisCard.image} alt={thisCard.name}></img>
                            <h3 id='name'>{thisCard.name}</h3>
                            <li><strong>Species: </strong>{thisCard.species}</li>
                            <li><strong>Origin: </strong>{thisCard.origin.name}</li>
                            <li><strong>Gender: </strong>{thisCard.gender}</li>
                            <li><strong>Status: </strong>{thisCard.status}</li>
                            <li><strong>Episodes: </strong>{thisCard.episode.length}</li>
                        </div>
                    </div>
                </div>
                
                
            )
        }
        
    }
} 
export default Profile;