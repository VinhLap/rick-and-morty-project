import React from 'react';
import styles from './Main.css';
import Card from '../Card/Card';

class Main extends React.Component{
    state = {
        pageNumber: 1,
        characters: [],
        filteredCharacter: [],
        searchbar: React.createRef(),
        myTable: React.createRef(),
        
    };
    async componentDidMount(n) {
        this.setState({characters: [], filteredCharacter: []})
        const api_url = `https://rickandmortyapi.com/api/character/?page=${n}`
        try {
          const response = await fetch(api_url).then(resp => resp.json());
          let chars = [...this.state.characters];
          chars.push(...response.results);
          this.setState({
            characters: chars, filteredCharacter: chars
          });
        } catch (e) {
          console.error(e);
        }
    }

    nextPage = () =>{
        let n = this.state.pageNumber
        n += 1;
        this.setState({pageNumber: n})
        this.componentDidMount(n);
    }
    prevPage = () =>{
        if(this.state.pageNumber !== 1){
            let n = this.state.pageNumber
            n -= 1;
            this.setState({pageNumber: n})
            this.componentDidMount(n);
        }  
    }

    filterSearch = () =>{
        let search = this.textInput.value.toUpperCase();
        let list = this.state.characters.filter(char=> char.name.toUpperCase().includes(search))
        this.setState({filteredCharacter: list})
        console.log(this.state.filteredCharacter[0])
    }

    findId(character){
        for(let i=0; i<this.state.characters.length; i++){
            if(this.state.characters[i].name === character.name){
                return this.state.characters[i]
            }
        }
        return null;
    }
    render() {
        // Create an array of JSX to render
        const filteredCharacters = this.state.filteredCharacter.map(character => {
          // This should render Character components. - Remember the key.
          return (
              <li id = "eachCard">
                  <Card class = 'card' character = {character} key = {character.id}/>
              </li>
          );
    });
        return (
            <div id='body'>
                <input type ='text' 
                    onChange = {() => this.filterSearch()} 
                    ref = {el => this.textInput = el} 
                    id = 'searchBar' placeholder = 'Search character'>
                </input>
                <ul className="characters">{filteredCharacters}</ul>
                <button id='btnPrev' onClick={() => this.prevPage()}>Prev</button>
                <button id='btnNext' onClick={() => this.nextPage()}>Next</button>
            </div>
        );
    }
}

export default Main;