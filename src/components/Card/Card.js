import React from 'react';
import { Link } from 'react-router-dom';
import styles from './Card.css'

class Card extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            name: this.props.character.name,
            id: this.props.character.id
        };
    }
    
    render() {
        return (
            
            <div>
                <Link className='link' to={`/profile/${this.state.id}`} key={this.state.id}>
                    <img className='image' src={this.props.character.image} alt={this.props.character.name}/>
                    <h4 className='name'>{this.props.character.name}</h4>
                </Link>
            </div> 
        )
    }
}

export default Card;